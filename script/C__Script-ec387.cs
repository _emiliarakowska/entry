
using System;
using System.Collections;
using System.Collections.Generic;

using Rhino;
using Rhino.Geometry;

using Grasshopper;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;

// Non-default includes.
using Rhino.DocObjects;
using Rhino.Collections;
using GH_IO;
using GH_IO.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;

/// Unique namespace, so visual studio won't throw any errors about duplicate definitions.
namespace nsec387
{
    /// <summary>
    /// This class will be instantiated on demand by the Script component.
    /// </summary>
    public class Script_Instance : GH_ScriptInstance
    {
        /// This method is added to prevent compiler errors when opening this file in visual studio (code) or rider.
        public override void InvokeRunScript(IGH_Component owner, object rhinoDocument, int iteration, List<object> inputs, IGH_DataAccess DA)
        {
            throw new NotImplementedException();
        }

        #region Utility functions
        /// <summary>Print a String to the [Out] Parameter of the Script component.</summary>
        /// <param name="text">String to print.</param>
        private void Print(string text) { /* Implementation hidden. */ }
        /// <summary>Print a formatted String to the [Out] Parameter of the Script component.</summary>
        /// <param name="format">String format.</param>
        /// <param name="args">Formatting parameters.</param>
        private void Print(string format, params object[] args) { /* Implementation hidden. */ }
        /// <summary>Print useful information about an object instance to the [Out] Parameter of the Script component. </summary>
        /// <param name="obj">Object instance to parse.</param>
        private void Reflect(object obj) { /* Implementation hidden. */ }
        /// <summary>Print the signatures of all the overloads of a specific method to the [Out] Parameter of the Script component. </summary>
        /// <param name="obj">Object instance to parse.</param>
        private void Reflect(object obj, string method_name) { /* Implementation hidden. */ }
        #endregion
        #region Members
        /// <summary>Gets the current Rhino document.</summary>
        private readonly RhinoDoc RhinoDocument;
        /// <summary>Gets the Grasshopper document that owns this script.</summary>
        private readonly GH_Document GrasshopperDocument;
        /// <summary>Gets the Grasshopper script component that owns this script.</summary>
        private readonly IGH_Component Component;
        /// <summary>
        /// Gets the current iteration count. The first call to RunScript() is associated with Iteration==0.
        /// Any subsequent call within the same solution will increment the Iteration count.
        /// </summary>
        private readonly int Iteration;
        #endregion
        /// <summary>
        /// This procedure contains the user code. Input parameters are provided as regular arguments,
        /// Output parameters as ref arguments. You don't have to assign output parameters,
        /// they will have a default value.
        /// </summary>
        #region Runscript
        private void RunScript(List<Box> voxels, ref object groups, ref object noise)
        {
            // add all box centers to tree with x,y,z path
            var BoxCenterTree = new DataTree<Box>();
            foreach (var box in voxels)
            {
                // simplify coordinates - create sectors for searching only in neibourhood 
                var x = Convert.ToInt32(box.Center.X);
                var y = Convert.ToInt32(box.Center.Y);
                var z = Convert.ToInt32(box.Center.Z);
                var path = new GH_Path(x, y, z);
                BoxCenterTree.Add(box, path);
            }

            var AllGroups = new List<HashSet<Box>>();
            var Visited = new HashSet<Box>();
            var NoiseCandidates = new HashSet<Box>();

            // for each box, check regions around (+2, 0, -2) for neighbours
            foreach (var box in voxels)
            {
                var GroupMembers = new HashSet<Box>();
                neighbours(ref BoxCenterTree, ref Visited, ref NoiseCandidates, ref GroupMembers, box);
                if (GroupMembers.Count > 1)
                {
                    AllGroups.Add(GroupMembers);
                }
            }

            // save results
            var ResultTree = new DataTree<Box>();
            var i = 0;
            foreach (var group in AllGroups)
            {
                ResultTree.AddRange(group.ToList(), new GH_Path(i++));
            }

            groups = ResultTree;
            noise = NoiseCandidates;
        }

        // find all neighours around one box
        private void neighbours(ref DataTree<Box> BoxCenterTree, ref HashSet<Box> Visited, ref HashSet<Box> NoiseCandidates, ref HashSet<Box> GroupMembers, Box Current)
        {
            if (Visited.Contains(Current)) return;
            if (NoiseCandidates.Contains(Current)) return;

            Visited.Add(Current);
            GroupMembers.Add(Current);

            var x = Convert.ToInt32(Current.Center.X);
            var y = Convert.ToInt32(Current.Center.Y);
            var z = Convert.ToInt32(Current.Center.Z);

            // looking around in minimal domain
            var HasNeighbours = false;
            for (int a = -2; a <= 2; a++)
            {
                for (int b = -2; b <= 2; b++)
                {
                    for (int c = -2; c <= 2; c++)
                    {
                        var searchPath = new GH_Path(x + a, y + b, z + c);
                        var branch = BoxCenterTree.Branch(searchPath);
                        if (branch != null)
                        { // branch exists, check all boxes
                            foreach (var Compared in branch)
                            {
                                var Distance = Current.Center.DistanceTo(Compared.Center);
                                if (!Current.Equals(Compared) && Distance == 0)
                                {
                                    // check for duplicates
                                    NoiseCandidates.Add(Compared);
                                }
                                // correct neighbours 
                                else if (Distance == 1
                                && (Current.Center.X == Compared.Center.X
                                || Current.Center.Y == Compared.Center.Y
                                || Current.Center.Z == Compared.Center.Z))
                                {
                                    HasNeighbours = true;
                                    neighbours(ref BoxCenterTree, ref Visited, ref NoiseCandidates, ref GroupMembers, Compared);
                                }
                            }
                        }
                    }
                }
            }

            if (!HasNeighbours) NoiseCandidates.Add(Current);
        }
        #endregion

        #region Additional

        #endregion
    }
}
